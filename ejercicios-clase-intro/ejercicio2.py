#!/usr/bin/env python

"""
Ejercicio 2.

Una persona adquirió un producto para pagar en 20 meses.
El primer mes pagó $1000, el segundo $2000, el tercero $4000
y ası́ sucesivamente. ¿cuánto debe pagar mensualmente y el total
de lo que pagó después de los 20 meses? utilice el ciclo apropiado.
"""

inicial = 1000
pagado = 0
total = 0
for indice in range(1, 21):
    pagado = inicial*(2**(indice-1))
    print(pagado)
    total = pagado + total
print("El total pagado es: ", total)
