#!/usr/bin/env python

"""
Ejercicio 2.

Una persona adquirió un producto para pagar en 20 meses.
El primer mes pagó $1000, el segundo $2000, el tercero $4000
y ası́ sucesivamente. ¿cuánto debe pagar mensualmente y el total
de lo que pagó después de los 20 meses? utilice el ciclo apropiado.
"""

inicial = 500
total = 0
# for indice in range(1, 21):
indice = 0
while indice < 20:
    inicial = 1000 * (2**indice)  # inicial * 2
    print("pago mensual", inicial)
    total = total + inicial
    indice = indice + 1
print("El pago total: ", total)
